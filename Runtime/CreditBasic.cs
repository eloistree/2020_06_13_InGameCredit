﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CreditBasic : I_BasicInformation
{

    [SerializeField] string m_pseudonym;
    [SerializeField] string m_howToContact;

    public string GetContactInfoAsText()
    {
        return m_howToContact;
    }

    public string GetFirstName()
    {
        return null;
    }

    public string GetLastName()
    {
        return null;
    }

    public string GetPseudonym()
    {
        return m_pseudonym;
    }

    public bool HasContactInfoAsText()
    {
        return string.IsNullOrEmpty(m_howToContact);
    }

    public bool HasFirstName()
    {
        return false;
    }

    public bool HasLastName()
    {
        return false;
    }

    public bool HasName()
    {
        return  false;
    }

    public bool HasPseudonym()
    {

        return string.IsNullOrEmpty(m_pseudonym);
    }

}
