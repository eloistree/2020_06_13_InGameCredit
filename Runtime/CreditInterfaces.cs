﻿

public interface I_BasicInformation {

    bool HasPseudonym();
    string GetPseudonym();
    
    bool HasName();
    bool HasFirstName();
    string GetFirstName();
    bool HasLastName();
    string GetLastName();

    bool HasContactInfoAsText();
    string GetContactInfoAsText();

}