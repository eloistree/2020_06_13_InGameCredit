﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickCredit : MonoBehaviour , I_BasicInformation
{
    public CreditBasic m_info;

    public string GetContactInfoAsText()
    {
       return  m_info.GetContactInfoAsText();
    }

    public string GetFirstName()
    {
        return m_info.GetFirstName();
    }

    public string GetLastName()
    {
        return m_info.GetLastName();
    }

    public string GetPseudonym()
    {
        return m_info.GetPseudonym();
    }

    public bool HasContactInfoAsText()
    {
        return m_info.HasContactInfoAsText();
    }

    public bool HasFirstName()
    {
        return m_info.HasFirstName();
    }

    public bool HasLastName()
    {
        return m_info.HasLastName();
    }

    public bool HasName()
    {
        return m_info.HasName();
    }

    public bool HasPseudonym()
    {
        return m_info.HasPseudonym();
    }
}
